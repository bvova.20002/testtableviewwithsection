//
//  SearchBarAndNav.swift
//  TestTableViewWithSection
//
//  Created by Hen Joy on 7/17/19.
//  Copyright © 2019 Hen Joy. All rights reserved.
//

import UIKit

class SearchBarAndNav: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    let arr = ["kek", "Shrek", "Cheburek", "Ivan", "Petro", "Gus","kek", "Shrek", "Cheburek", "Ivan", "Petro", "Gus","kek", "Shrek", "Cheburek", "Ivan", "Petro", "Gus","kek", "Shrek", "Cheburek", "Ivan", "Petro", "Gus","kek", "Shrek", "Cheburek", "Ivan", "Petro", "Gus"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavBar()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.textLabel?.text = arr[indexPath.row]
        return cell
    }

    func setupNavBar() {
        
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.placeholder = "Поиск упражнений...>"
        searchController.searchBar.sizeToFit()
        navigationItem.searchController = searchController
    }

}

//
//  TestTBVC.swift
//  TestTableViewWithSection
//
//  Created by Hen Joy on 7/11/19.
//  Copyright © 2019 Hen Joy. All rights reserved.
//

import UIKit

struct Exercise {
    let name: String?
    let parent: String?
}

class TestTBVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var segController: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    
    var exerciseDictionary = [String: [String]]()
    var muscleSectionTitles = [String]()
    var muscles = ["Грудь", "Спина", "Плечи", "Ноги", "Бицепс", "Вис"]
    var home = [Exercise(name: "Отжимания", parent: "грудь"), Exercise(name: "Жим", parent: "Грудь"), Exercise(name: "Брусья", parent: "Грудь"), Exercise(name: "тяга спины", parent: "спина"), Exercise(name: "Спина тест", parent: "Спина"), Exercise(name: "перед дельта", parent: "плечи"), Exercise(name: "Присед", parent: "ноги"), Exercise(name: "Подтягивания", parent: "бицепс"), Exercise(name: "vis Test", parent: "вис")]
    var Gym = [Exercise(name: "Отжимания2", parent: "грудь"), Exercise(name: "Жим2", parent: "Грудь"), Exercise(name: "Брусья2", parent: "Грудь"), Exercise(name: "тяга спины2", parent: "спина"), Exercise(name: "Спина тест2", parent: "Спина"), Exercise(name: "перед дельта2", parent: "плечи"), Exercise(name: "Присед2", parent: "ноги"), Exercise(name: "Подтягивания2", parent: "бицепс"), Exercise(name: "vis Test2", parent: "вис")]
    var workout = [Exercise(name: "Отжимания3", parent: "грудь"), Exercise(name: "Жим3", parent: "Грудь"), Exercise(name: "Брусья3", parent: "Грудь"), Exercise(name: "тяга спины3", parent: "спина"), Exercise(name: "Спина тест3", parent: "Спина"), Exercise(name: "перед дельта3", parent: "плечи"), Exercise(name: "Присед3", parent: "ноги"), Exercise(name: "Подтягивания3", parent: "бицепс"), Exercise(name: "vis Test3", parent: "вис")]
    
    lazy var locations = [home, Gym, workout]
    lazy var CurrentData = locations[segController.selectedSegmentIndex]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // enum work example
        if let typeTest = typeExercise(rawValue: "kg/count") {
            print(typeTest.rawValue)
        }
        if let tptest = typeExercise(rawValue: "str") {
            print(tptest.rawValue)
        } else {
            print("notfound")
        }
        //enum work example
        tableView.delegate = self
        tableView.dataSource = self
        fillDataMuscles()
        fillDataArrays()
        
       
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return muscleSectionTitles.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        let carKey = muscleSectionTitles[section]
        if let carValues = exerciseDictionary[carKey] {
            return carValues.count
        }
        
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // 3
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        // Configure the cell...
        let carKey = muscleSectionTitles[indexPath.section]
        if let carValues = exerciseDictionary[carKey] {
            cell.textLabel?.text = carValues[indexPath.row]
        }
        
        return cell
    }
 
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return muscleSectionTitles[section]
    }
    
    func fillDataMuscles() {
        var mus = [String]()
        for loc in locations {
            for ex in loc {
                if !mus.contains(where: { (str) -> Bool in
                    if str.lowercased() == ex.parent!.lowercased(){
                        return true
                    }
                    return false
                }) {
                    mus.append(ex.parent!.capitalizingFirstLetter())
                }
            }
        }
        muscles = mus
        print(muscles)
    }
    
    func fillDataArrays() {
        exerciseDictionary = [String: [String]]()
        for muscle in muscles {
            let carKey = muscle
            for ex in CurrentData {
                if ex.parent!.lowercased() == muscle.lowercased() {
                    if var carValues = exerciseDictionary[carKey] {
                        carValues.append(ex.name!)
                        exerciseDictionary[carKey] = carValues
                    } else {
                        exerciseDictionary[carKey] = [ex.name!]
                    }
                }
            }
        }
        muscleSectionTitles = [String](exerciseDictionary.keys)
        muscleSectionTitles = muscleSectionTitles.sorted(by: { $0 < $1 })
    }
    
    @IBAction func segValueChanged(_ sender: Any) {
        
        switch segController.selectedSegmentIndex {
        case 3:
            tableView.isHidden = true
        default:
            tableView.isHidden = false
            CurrentData = locations[segController.selectedSegmentIndex]
            fillDataArrays()
            tableView.reloadData()
        }
    }
    
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}

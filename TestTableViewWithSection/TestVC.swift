//
//  TestVC.swift
//  TestTableViewWithSection
//
//  Created by Hen Joy on 7/16/19.
//  Copyright © 2019 Hen Joy. All rights reserved.
//

import UIKit

enum typeExercise: String {
    case count = "count"
    case kg = "kg/count"
    case time = "time"
}

class TryExerciseView {
    
}

class TestVC : UIViewController, UISearchBarDelegate, UITextFieldDelegate {
    var searchBar = UISearchBar()
    var searchBarButtonItem: UIBarButtonItem?
    var logoImageView: UIView!
    var searchButton: UIBarButtonItem?
    
    
    @IBOutlet weak var imageView: UIImageView!
    
    
    var curYPos: CGFloat = 0
    var currentTryPossition = 0
    var curCountTry: Int = 1
    var tryViews = [UIView]()
    var countTextFields = [UITextField]()
    var kgTextFields = [UITextField]()
    var buttonArray = [UIButton]()
    var typeLabelsArray = [UILabel]()
    var numbLabelsArray = [UILabel]()
    var countPlaceholder = [11,22,33,44,55,66,77,88,99]
    var kgPlaceholder = [11,12,13,14,15,16,17,18,19]
    var checkboxArray = [Checkbox]()
    @IBOutlet weak var viewTryConst: NSLayoutConstraint!
    @IBOutlet weak var countConst: NSLayoutConstraint!
    @IBOutlet weak var viewTry: UIView!
    @IBOutlet weak var headerViewTry: UIView!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var numLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var kgLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        logoImageView = navigationItem.titleView
        searchBar.delegate = self
        searchBar.searchBarStyle = UISearchBar.Style.minimal
        searchBar.showsCancelButton = true
        searchBarButtonItem = navigationItem.rightBarButtonItem
        
        stopButton.layer.cornerRadius = 20
        
        curYPos = headerViewTry.frame.height
        let typeEX = typeExercise.time
        
        if typeEX != .kg {
            kgLabel.isHidden = true
        }
        createTry(count: 4, typeEX: typeEX)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.length + range.location > textField.text!.count {
            return false
        }
        
        let newLimite = textField.text!.count + string.count - range.length
        return newLimite <= 4
    }
    
    @objc func showSearchBar() {
        navigationItem.rightBarButtonItem = UIBarButtonItem()
        navigationItem.titleView = searchBar
        UIView.animate(withDuration: 0.5, animations: {
            self.searchBar.alpha = 1
        }, completion: { finished in
            self.searchBar.becomeFirstResponder()
        })
    }
    
    func hideSearchBar() {
        UIView.animate(withDuration: 0.3, animations: {
            self.searchBar.alpha = 0
        }, completion: { finished in
            self.navigationItem.setRightBarButton(self.searchBarButtonItem, animated: true)
            self.navigationItem.titleView = self.logoImageView
            
        })
    }
    
    
    //MARK: UISearchBarDelegate
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        hideSearchBar()
    }
    
    func createTry(count: Int, typeEX: typeExercise) {
        if typeEX == .time && curCountTry == 1{
            countConst.constant = countLabel.frame.width + 30
        }
        
        view.layoutIfNeeded()
        for _ in 1...count {
            view.layoutIfNeeded()
            let heightLabel = numLabel.frame.height + 10
            let heightView:CGFloat = 50
            let v = UIView(frame: CGRect(x: 0, y: curYPos, width: headerViewTry.frame.width, height: heightView))
            
            let lNumb = UILabel(frame: CGRect(x: numLabel.frame.origin.x + 5, y: 10, width: numLabel.frame.width - 10, height: heightLabel))
            let lType = UILabel(frame: CGRect(x: typeLabel.frame.origin.x + 5, y: 10, width: typeLabel.frame.width - 10, height: heightLabel))
            let lKg = UITextField(frame: CGRect(x: kgLabel.frame.origin.x + 5, y: 10, width: kgLabel.frame.width - 10, height: heightLabel))
            let lCount = UITextField(frame: CGRect(x: countLabel.frame.origin.x + 5, y: 10, width: countLabel.frame.width - 10, height: heightLabel))
            let labels = [lNumb,lType]
            for label in labels {
                var str = ""
                if label == lNumb {
                    str = String(curCountTry)
                } else {
                    str = typeEX.rawValue
                }
                label.text = str
                label.textAlignment = .center
                label.textColor = .white
                let sizeFont = label == lNumb ? 18 : 15
                label.font = UIFont.boldSystemFont(ofSize: CGFloat(sizeFont))
                v.addSubview(label)
            }
            var textfields = [UITextField]()
            if typeEX == .kg {
                textfields.append(lKg)
            } else {
                if typeEX == .count {
                    
                } else {
                    lCount.frame.size.width = lCount.frame.width - 30
                    lCount.frame.origin.x = lCount.frame.origin.x + 15
                }
            }
            textfields.append(lCount)
            for tf in textfields {
               
                    
                    if tf == lKg {
                        if kgPlaceholder.count >= curCountTry {
                            tf.attributedPlaceholder = NSAttributedString(string: String(kgPlaceholder[curCountTry - 1]),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
                        }
                        kgTextFields.append(tf)
                    } else {
                        if kgPlaceholder.count >= curCountTry {
                            tf.attributedPlaceholder = NSAttributedString(string: String(countPlaceholder[curCountTry - 1]),
                                                                          attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
                        }
                       
                        countTextFields.append(lCount)
                    }
                
                
                if currentTryPossition + 1 != curCountTry {
                    tf.isEnabled = false
                    v.backgroundColor = .darkGray
                }
                tf.textAlignment = .center
                tf.textColor = .white
                tf.clipsToBounds = true
                tf.layer.cornerRadius = numLabel.frame.height / 2
                tf.layer.borderColor = UIColor.white.cgColor
                tf.layer.borderWidth = 2
                tf.backgroundColor = .darkGray
                tf.font = .systemFont(ofSize: 15)
                tf.borderStyle = .roundedRect
                tf.autocorrectionType = .no
                tf.keyboardType = .numberPad
                tf.returnKeyType = .done
                tf.delegate = self
                v.addSubview(tf)
            }
            if typeEX == .time {
                let but = UIButton(frame: lCount.frame)
                
                if currentTryPossition + 1 != curCountTry {
                    but.isEnabled = false
                }
                but.frame.size.width = but.frame.width - 15
                but.setTitle("Начать", for: .normal)
                but.titleLabel?.font = .systemFont(ofSize: 12)
                but.backgroundColor = .init(red: 13/255, green: 131/255, blue: 15/255, alpha: 1)
                but.setTitleColor(.white, for: .normal)
                but.frame.origin.x = headerViewTry.frame.width - but.frame.width - 5
                but.addTarget(self, action: #selector(startBut), for: .touchUpInside)
                but.clipsToBounds = true
                but.layer.cornerRadius = but.frame.height / 2
                buttonArray.append(but)
                v.addSubview(but)
                
            } else {
                let but = Checkbox(frame: lCount.frame)
                
                if currentTryPossition + 1 != curCountTry {
                    but.isEnabled = false
                }
                but.borderStyle = .square
                but.checkmarkStyle = .tick
                but.layer.cornerRadius = 7
                but.layer.borderWidth = 2
                but.layer.borderColor = UIColor.white.cgColor
                but.backgroundColor = .darkGray
                but.clipsToBounds = true
                but.uncheckedBorderColor = .white
                but.checkedBorderColor = .white
                but.checkmarkSize = 0.7
                but.checkmarkColor = .white
                but.valueChanged = { (value) in
                    if value {
                        
                        self.currentTryPossition += 1
                        if lCount.text == "" || (lKg.text == "" && typeEX == .kg){
                            but.isChecked = !value
                            lCount.backgroundColor = .red
                            lKg.backgroundColor = .red
                            return
                        }
                        lCount.backgroundColor = .darkGray
                        lKg.backgroundColor = .darkGray
                        if self.countPlaceholder[self.currentTryPossition] < Int(lCount.text!)!{
                            if typeEX == .kg && Int(lKg.text!)! < self.kgPlaceholder[self.currentTryPossition]{
                                v.backgroundColor = .red
                            } else {
                                v.backgroundColor = .green
                            }
                            v.backgroundColor = .green
                        } else {
                            v.backgroundColor = .red
                        }
                        lCount.isEnabled = false
                        lKg.isEnabled = false
                        
                        if self.currentTryPossition < self.countTextFields.count {
                            self.tryViews[self.currentTryPossition].backgroundColor = .darkText
                            self.countTextFields[self.currentTryPossition].isEnabled = true
                            self.checkboxArray[self.currentTryPossition].isEnabled = true
                            if self.kgTextFields.count > 0 {
                                self.kgTextFields[self.currentTryPossition].isEnabled = true
                            }
                        } else {
                            print("create new")
//                            self.createTry(count: 1, typeEX: .count)
                        }
                        
                        lNumb.textColor = .black
                        lType.textColor = .black
                        print("Number \(lNumb.text!) isChecked = \(value)")
                    } else {
                        self.currentTryPossition = Int(lNumb.text!)! - 1
                        let forLoop = self.currentTryPossition + 1
                        for i in forLoop..<self.curCountTry - 1 {
                            self.tryViews[i].backgroundColor = .darkGray
                            self.typeLabelsArray[i].textColor = .white
                            self.numbLabelsArray[i].textColor = .white
                            self.countTextFields[i].isEnabled = false
                            self.checkboxArray[i].isEnabled = false
                            self.checkboxArray[i].isChecked = false
                            if self.kgTextFields.count > 0 {
                                self.kgTextFields[i].isEnabled = false
                            }
                        }
                        v.backgroundColor = .darkText
                        lCount.isEnabled = true
                        lKg.isEnabled = true
                        lNumb.textColor = .white
                        lType.textColor = .white
                    }
                }
                but.frame.size.width = but.frame.height
                but.frame.origin.x = headerViewTry.frame.width - but.frame.width - 5
                checkboxArray.append(but)
                v.addSubview(but)
            }
            typeLabelsArray.append(lType)
            numbLabelsArray.append(lNumb)
            tryViews.append(v)
            viewTry.addSubview(v)
            curYPos += heightView
            curCountTry += 1
        }
        viewTryConst.constant = curYPos
        
        self.view.layoutIfNeeded()
        scrollView.setContentOffset(CGPoint(x: 0, y: self.scrollView.contentSize.height - self.scrollView.bounds.size.height + self.scrollView.contentInset.bottom), animated: true)
    }
    
    @objc func startBut() {
        if countTextFields[currentTryPossition].text != "" {
            
            tryViews[currentTryPossition].backgroundColor = .green
            countTextFields[currentTryPossition].backgroundColor = .darkGray
            countTextFields[currentTryPossition].isEnabled = false
            numbLabelsArray[currentTryPossition].textColor = .black
            typeLabelsArray[currentTryPossition].textColor = .black
            buttonArray[currentTryPossition].isEnabled = false
            currentTryPossition += 1
            if currentTryPossition < countTextFields.count {
                countTextFields[currentTryPossition].isEnabled = true
                buttonArray[currentTryPossition].isEnabled = true
                tryViews[currentTryPossition].backgroundColor = .black
            } else {
                createTry(count: 1, typeEX: .time)
            }
        } else {
            countTextFields[currentTryPossition].backgroundColor = .red
        }
        
    }
    
    @IBAction func addButtonPressed(_ sender: Any) {
        createTry(count: 1, typeEX: .kg)
    }
    
    @IBAction func searchButtonPressed(sender: Any) {
        showSearchBar()
    }
    
}
